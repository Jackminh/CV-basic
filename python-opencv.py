import cv2

# read image
image = cv2.imread('familychaos.jpg')
height, width = image.shape[:2]
print(height, width)
# save image
cv2.imwrite('familychaos_output.png',image)

# convert into gray

# gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
# cv2.imshow("Image showing", gray)
# cv2.waitKey()

# setup video capture
cap = cv2.VideoCapture(0)
# get frame, apply Gaussian smoothing, show result
while True:
    ret,im = cap.read()
    blur = cv2.GaussianBlur(im,(0,0),5)
    cv2.imshow('camera blur', blur)
    if cv2.waitKey(10) == 27:
        break